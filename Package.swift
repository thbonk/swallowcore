// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwallowCore",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SwallowCore",
            type: .dynamic, 
            targets: ["SwallowCore"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/SwifterSwift/SwifterSwift.git", from: "5.2.0"),
        .package(url: "https://github.com/Swinject/Swinject.git", from: "2.7.1"),
        .package(url: "https://github.com/Swinject/SwinjectAutoregistration.git", from: "2.7.0"),
        .package(url: "https://github.com/Quick/Quick.git", from: "3.0.0"),
        .package(url: "https://github.com/Quick/Nimble.git", from: "8.1.2")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SwallowCore",
            dependencies: ["SwifterSwift", "Swinject", "SwinjectAutoregistration"]),
        .testTarget(
            name: "SwallowCoreTests",
            dependencies: ["SwallowCore", "Quick", "Nimble"],
            resources: [
                .process("TestResources/Localizable.strings")
            ])
    ]
)
