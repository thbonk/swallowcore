# SwallowCore
Core framework for Swallow that provides classes and functions for:
- Dependency Injection
- Plugin Management
- Logging