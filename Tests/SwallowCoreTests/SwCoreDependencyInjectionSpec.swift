/*
   SwCoreDependencyInjectionSpec.swift
   SwallowCoreTests

   Copyright 2020 Swallow.io Development Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import Quick
import Nimble

@testable import SwallowCore

final class SwCoreDependencyInjectionSpec: QuickSpec {
    override func spec() {
        
        describe("Registering and resolving services") {
            beforeEach {
                SwCoreGlobalContext.resetServiceRegistry()
            }

            it("Registering and resolving a singleton service always results in the same object") {

                SwCoreGlobalContext.register(service: Person.self) { Person() }

                let pers1 = SwCoreGlobalContext.resolve(service: Person.self)
                let pers2 = SwCoreGlobalContext.resolve(service: Person.self)

                expect(pers1! === pers2!).to(be(true))
            }

            it("Registering and resolving a prototype service always results in different objects") {

                SwCoreGlobalContext.register(service: Person.self, scope: .prototype) { Person() }

                let pers1 = SwCoreGlobalContext.resolve(service: Person.self)
                let pers2 = SwCoreGlobalContext.resolve(service: Person.self)

                expect(pers1! === pers2!).to(be(false))
            }

            it("Registering and resolving a named singleton service always results in the same object") {

                SwCoreGlobalContext.register(service: Person.self, name: "John") { Person(firstname: "John", lastname: "Doe") }

                let pers1 = SwCoreGlobalContext.resolve(service: Person.self, name: "John")
                let pers2 = SwCoreGlobalContext.resolve(service: Person.self, name: "John")

                expect(pers1! === pers2!).to(be(true))
            }

            it("Registering and resolving a named prototype service always results in different objects") {

                SwCoreGlobalContext.register(service: Person.self, name: "John", scope: .prototype) { Person(firstname: "John", lastname: "Doe") }

                let pers1 = SwCoreGlobalContext.resolve(service: Person.self, name: "John")
                let pers2 = SwCoreGlobalContext.resolve(service: Person.self, name: "John")

                expect(pers1! === pers2!).to(be(false))
            }

            it("Dependency injection via initializer is successful") {

                SwCoreGlobalContext.register(service: Person.self) { Person(firstname: "John", lastname: "Doe") }
                SwCoreGlobalContext.register(service: Vehicle.self, initializer: Vehicle.init(driver:))

                let pers = SwCoreGlobalContext.resolve(service: Person.self)
                let vehicle = SwCoreGlobalContext.resolve(service: Vehicle.self)

                expect(vehicle!.driver === pers!).to(be(true))
            }
        }

        describe("Interaction between parent and child contexts") {
            beforeEach {
                SwCoreGlobalContext.resetServiceRegistry()
            }

            class PrivateContext: SwCoreContext {
                init() {
                    super.init(parent: SwCoreGlobalContext)
                }
            }

            it("Dependency injection via initializer is successful") {
                let privateContext = PrivateContext()

                SwCoreGlobalContext.register(service: Person.self) { Person(firstname: "John", lastname: "Doe") }
                privateContext.register(service: Vehicle.self, initializer: Vehicle.init(driver:))

                let pers = SwCoreGlobalContext.resolve(service: Person.self)
                let vehicle = privateContext.resolve(service: Vehicle.self)

                expect(vehicle!.driver === pers!).to(be(true))
            }

            it("Resolving all services accross contexts is successful") {
                let privateContext = PrivateContext()

                SwCoreGlobalContext.register(service: Person.self, name: "John") { Person(firstname: "John", lastname: "Doe") }
                privateContext.register(service: Person.self, name: "Jane") { Person(firstname: "Jane", lastname: "Doe") }

                let persons = privateContext.resolveAll(service: Person.self)

                expect(persons.count).to(equal(2))
                expect(persons).to(containElementSatisfying({ person in
                    return person.firstname == "John" && person.lastname == "Doe"
                }))
                expect(persons).to(containElementSatisfying({ person in
                    return person.firstname == "Jane" && person.lastname == "Doe"
                }))
            }
        }
    }
}
