/*
  SwCoreLocalizationSpec.swift
  SwallowCoreTests

  Copyright 2020 Swallow.io Development Team

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

import Foundation
import Quick
import Nimble

@testable import SwallowCore

final class SwCoreLocalizationSpec: QuickSpec {
    override func spec() {
        beforeEach {
            let localizationRegistry =
                SwCoreGlobalContext
                    .resolve(service: SwCoreLocalizationRegistry.self)

            localizationRegistry?
                .register(localizationBundle: Bundle.module)
        }
        
        describe("Localization AP") {

            it("Localizing simple string is successful") {
                expect("test".localized).to(equal("localized"))
            }

            it("Localizating a missing key returns the key") {
                expect("missing".localized).to(equal("missing"))
            }

            it("Localizing a string with parameters is successful") {
                expect("parameters".localized(with: ["name": "Fred"])).to(equal("Name is Fred."))
            }

            it("Localizing a string with a count is successful") {
                expect("count".localized(count: 0)).to(equal("none"))
                expect("count".localized(count: 1)).to(equal("one"))
                expect("count".localized(count: 2)).to(equal("2"))
            }

            it("Localizing a string with a count and a selection is succesful") {
                expect("selection".localized(count: 2, selected: 0)).to(equal("0 of 2 selected"))
                expect("selection".localized(count: 2, selected: 1)).to(equal("1 of 2 selected"))
                expect("selection".localized(count: 2, selected: 2)).to(equal("All 2 selected"))
            }
        }
    }
}
