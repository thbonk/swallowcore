/*
   SwCoreObjectSpec.swift
   SwallowCoreTests

   Copyright 2020 Swallow.io Development Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import Foundation
import Quick
import Nimble

@testable import SwallowCore

final class SwCoreObjectSpec: QuickSpec {

    override func spec() {
        describe("Instantiating SwCoreObject") {

            it("A SwCoreObject has an ObjectId and a CreatedAt timestamp") {
                let obj = SwCoreObject()

                expect(obj.objId).to(beAKindOf(UUID.self))
                expect(obj.createdAt).to(beAKindOf(Date.self))
            }

            it("Custom object tracer is called") {
                class ObjectTracer: SwCoreObjectTracer {
                    var initializedObject: SwCoreObjectProtocol!

                    func initialized(object: SwCoreObjectProtocol) {
                        initializedObject = object
                    }

                    func deinitialized(object: SwCoreObjectProtocol) {

                    }

                    init() {}
                }

                SwCoreGlobalObjectTracer = ObjectTracer()

                let obj = SwCoreObject()
                expect(obj.objId).to(equal((SwCoreGlobalObjectTracer as! ObjectTracer).initializedObject.objId))
            }
        }
    }
}
