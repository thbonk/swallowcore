/*
    SwCoreLocalization.swift
    SwallowCore

    Copyright 2020 Swallow.io Development Team

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import Foundation

/**
 This is the localization registry where other components of the Swallow and
 and an app can register their bundles that contain localizations.

 This is inspired by https://github.com/elegantchaos/Localization
 */
public final class SwCoreLocalizationRegistry: SwCoreObject {

    // MARK: - Public Properties

    /// This array references all bundles that provide localizations.
    public private(set) var localizationBundles: [Bundle] = []


    // MARK: - Initialization

    internal override init() {
        super.init()
    }


    // MARK: - API

    /// Register a localization bundle.
    ///
    /// - Parameters:
    ///   - localizationBundle: The bundle that shall be registered
    public func register(localizationBundle bundle: Bundle) {
        localizationBundles.append(bundle)
    }
}

public extension String {

    /// Look up a simple localized version of the string, using the default search strategy.
    var localized: String {
        return localized(with: [:])
    }

    /**
     Look up a localized version of the string.

     If a bundle is specified, we only search there. If no bundle is specified, we search in a set
     of registered bundles. This always includes the main bundle, but can have other bundles added
     to it, allowing you to automatically pick up translations from framework bundles (without having
     to search through every loaded bundle).

     - Parameters:
       - with: The arguments that shall replace the correspnding placeholders in the string
       - tableName: The name of the table containing the localized string
         identified by this string. This is the prefix of the strings file—a file with
         the `.strings` extension—containing the localized values. If `tableName`
         is `nil` or the empty string, the `Localizable` table is used.
       - bundle: The bundle containing the table's strings file. The main registered
         bundles are used by default.
       - value: A user-visible string to return when the localized string
         for this string cannot be found in the table. If `value` is the empty string,
         this string would be returned instead.
       - comment: A note to the translator describing the context where
         the localized string is presented to the user.

     - Returns: A localized version of the string designated by `this string in the
       table identified by `tableName`. If the localized string for this string cannot
       be found within the table, `value` is returned. However, this string is returned
       instead when `value` is the empty string.
     */
    func localized(
        with args: [String:Any] = [:],
        tableName: String? = nil,
           bundle: Bundle? = nil,
            value: String = "",
          comment: String = "") -> String {

        var string = self
        let localizationRegistry = SwCoreGlobalContext.resolve(service: SwCoreLocalizationRegistry.self)
        let bundlesToSearch = (bundle == nil) ? localizationRegistry!.localizationBundles : [bundle!]

        for bundle in bundlesToSearch {
            string = NSLocalizedString(self, tableName: tableName, bundle: bundle, value: value, comment: comment)

            if string != self {
                break
            }
        }

        for (key, value) in args {
            string = string.replacingOccurrences(of: "{\(key)}", with: String(describing: value))
        }

        return string
    }


    /**
     Return a count string. The exact text is pulled from the translation,
     but is generally of the form "x entit(y/ies)", or "no entities".

     If a selection count is also supplied, the string is expected to
     instead be of the form "All x selected".

     - Parameters:
       - count: The count
       - selected: The selected number
       - tableName: The name of the table containing the localized string
         identified by this string. This is the prefix of the strings file—a file with
         the `.strings` extension—containing the localized values. If `tableName`
         is `nil` or the empty string, the `Localizable` table is used.
       - bundle: The bundle containing the table's strings file. The main registered
         bundles are used by default.
       - value: A user-visible string to return when the localized string
         for this string cannot be found in the table. If `value` is the empty string,
         this string would be returned instead.
       - comment: A note to the translator describing the context where
         the localized string is presented to the user.

     - Returns: The string with the replaced count or selected placeholders.
     */
    func localized(
            count: Int,
         selected: Int = 0,
        tableName: String? = nil,
           bundle: Bundle? = nil,
            value: String = "",
          comment: String = "") -> String {

        var key = self

        if count > 0 && count == selected {
            key += ".all"
        } else {
            switch count {
            case 0: key += ".none"
            case 1: key += ".singular"
            default: key += ".plural"
            }
        }

        return
            key
                .localized(
                         with: ["count": count, "selected": selected],
                    tableName: tableName,
                       bundle: bundle,
                        value: value,
                      comment: comment)
    }
}
