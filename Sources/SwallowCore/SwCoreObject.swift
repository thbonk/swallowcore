/*
   SwCoreObject.swift
   SwallowCore

   Copyright 2020 Swallow.io Development Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import Foundation

/**
 * This protocol defines an interface that must be implemented by object tracers.
 *
 * An object tracer is called whenever an object is initialized or deinitizialized.
 * This supports debugging during development.
 *
 * Author: Thomas Bonk <thomas@meandmymac.de>
 */
public protocol SwCoreObjectTracer {
    /**
     * This method is called whenever an object is initialized.
     *
     * Parameter object: The object that is initialized.
     */
    func initialized(object: SwCoreObjectProtocol)

    /**
     * This method is called whenever an object is deinitialized.
     *
     * Parameter object: The object that is deinitialized.
     */
    func deinitialized(object: SwCoreObjectProtocol)
}

/**
 * This is the default object tracer which is a singleton.
 *
 * The singleton is registered as the global object tracer. By default it does nothing,
 * when the methods initialized(object:) or deinitialized(object:) are called.
 * You can enable logging of initialization or deinitialization by setting the 
 * property loggingEnabled to true.
 *
 * Author: Thomas Bonk <thomas@meandmymac.de>
 */
public final class SwCoreDefaultObjectTracer: SwCoreObjectTracer {
    
    // MARK: - Properties

    /// The shared SwCoreDefaultObjectTracer object.
    public static let shared = SwCoreDefaultObjectTracer()

    /// The flag that determines whether initialization or deinitialization of
    /// objects shall be logged. The default value is false.
    public var loggingEnabled = false


    // MARK: - Initialization

    private init() {
        // Empty by design
    }


    // MARK: - SwCoreObjectTracer

    /**
      * This method is called whenever an object is initialized.
      *
      * If loggingEnabled is true, the object's description is logged using NSLog.
      *
      * Parameter object: The object that is initialized.
      */
    public func initialized(object: SwCoreObjectProtocol) {
        if loggingEnabled {
            NSLog("Initialized object \(object.description)")
        }
    }

    /**
      * This method is called whenever an object is deinitialized.
      *
      * If loggingEnabled is true, the object's description is logged using NSLog.
      *
      * Parameter object: The object that is deinitialized.
      */
    public func deinitialized(object: SwCoreObjectProtocol) {
        if loggingEnabled {
            NSLog("Deinitialized object \(object.description)")
        }
    }
}

/// This is the global object tracer. By default, the singleton SwCoreDefaultObjectTracer
/// is registered.
public var SwCoreGlobalObjectTracer: SwCoreObjectTracer? = SwCoreDefaultObjectTracer.shared

/**
 * This protocol defines the interface to which a SwCoreObject must adhere to.
 *
 * If your class can't inherit SwCoreObject, it should implement this protocol.
 * You should add a at least one initializer to your class. All initializers
 * shall call the method initialized(). 
 * Additionally you should add a deinitializer that shall call the method
 * deinitialized().
 *
 * Author: Thomas Bonk <thomas@meandmymac.de>
 */
public protocol SwCoreObjectProtocol {
    
    // MARK: - Properties

    /// The id of the object.
    var objId: UUID { get }

    // The date and time, when this object has been created.
    var createdAt: Date { get }

    /// The description of the object
    var description: String { get }


    /// MARK: - Methods

    /// This method shall be called when the object is initialized.
    func initialized()

    /// This method shall be called when the object is deinitialized.
    func deinitialized()
}

public extension SwCoreObjectProtocol {

    func initialized() {
        SwCoreGlobalObjectTracer?.initialized(object: self)
    }

    func deinitialized() {
        SwCoreGlobalObjectTracer?.deinitialized(object: self)
    }
}

/**
 * This is the base class for all Swallow classes.
 *
 * This class provides properties for the object id and the timestamp when this object has been created.
 * Additionally it calls the method SwCoreObjectTracer.instantiated(object:) when an object is instatiated and
 * SwCoreObjectTracer.deinitialized(object:) when the object has been deinitialized.
 *
 * Author: Thomas Bonk <thomas@meandmymac.de>
*/
open class SwCoreObject: SwCoreObjectProtocol {

    // MARK: - Public Properties

    /// The id of the object.
    public let objId = UUID()

    /// The date and time, when this object has been created.
    public let createdAt = Date()

    /// The description of the object
    public var description: String {
        return "{Class: \(type(of: self)), ObjectId: \(self.objId), CreatedAt: \(self.createdAt)}"
    }


    // MARK: - Initialization and Deinitialization

    /// Default initializer, that calls the SwCoreGlobalObjectTracer.
    public init() {
        initialized()
    }

    /// Denitializer, that calls the SwCoreGlobalObjectTracer.
    deinit {
        deinitialized()
    }
}
