/*
 SwCoreLogFormatter.swift
 SwallowCore

 Copyright 2020 Swallow.io Development Team

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

import Foundation

/**
 The log formatter receives a log message and converts it to a string.
 The string is then being send to a writer.
 */
open class SwCoreLogFormatter: SwCoreObject {

    // MARK: - Initialization

    /// Default constructor that initializes the log formatter.
    public override init() {
        super.init()
    }


    // MARK: - Formatter API

    /// This methods formats the given message to a string.
    ///
    /// The returned string has the format:
    ///
    /// ```\(message.timestamp)|\(message.logger)|\(message.logLevel.description)|\(message.message)```
    ///
    /// If the message has log level `trace` the following string is appended:
    ///
    /// ```|\(message.filename):\(message.line):\(message.column)|\(message.function)```
    ///
    /// - Parameters:
    ///     - message the message object to be formatted
    ///
    /// - Returns: The formatted message as string
    public func format(message: SwCoreLogMessage) -> String {
        var msg =  "\(message.timestamp)|\(message.logger)|\(message.logLevel.description)|\(message.message)"

        if message.logLevel == .trace {
            msg += "|\(message.filename):\(message.line):\(message.column)|\(message.function)"
        }

        return msg
    }
}
