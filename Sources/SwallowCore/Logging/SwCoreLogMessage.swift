/*
 SwCoreLogMessage.swift
 SwallowCore

 Copyright 2020 Swallow.io Development Team

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

import Foundation

/**
 This class encapsulates all properties of a log entry.
 Instances of this class are immutable.
 */
public final class SwCoreLogMessage: SwCoreObject {

    // MARK: - Properties

    /// The name of the logger
    public private(set) var logger: String

    /// The log level of the log message
    public private(set) var logLevel: SwCoreLogLevel

    /// The message to output
    public private(set) var message: String

    /// The timestampü of the log message
    public private(set) var timestamp: Date

    /// The filename where the log message was raised
    public private(set) var filename: String

    /// The line number where the log message was raised
    public private(set) var line: Int

    /// The column where the log message was raised
    public private(set) var column: Int

    /// The function where the log message was raised
    public private(set) var function: String


    // MARK: - Initialization

    /// Initializer that takes the properties of the message.
    ///
    /// - Parameters:
    ///   - logger: Name of the logger
    ///   - logLevel: Log level of the message
    ///   - message: Log message
    ///   - timestamp: Timestamp of the log message
    ///   - filename: Name of the source file where the message has been logged
    ///   - line: Line in the source file where the message has been logged
    ///   - column: Column in the source file where the message has been logged
    ///   - function: Function where the message has been logged
    public init(
        logger: String,
        logLevel: SwCoreLogLevel,
        message: String,
        timestamp: Date,
        filename: String,
        line: Int,
        column: Int,
        function: String) {

        self.logger = logger
        self.logLevel = logLevel
        self.message = message
        self.timestamp = timestamp
        self.filename = filename
        self.line = line
        self.column = column
        self.function = function
        
        super.init()
    }
}
