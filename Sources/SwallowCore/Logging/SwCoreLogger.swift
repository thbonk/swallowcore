/*
   SwCoreLogger.swift
   SwallowCore

   Copyright 2020 Swallow.io Development Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import Foundation
import SwifterSwift


/**
 * All available log levels. They are hierarchical, i.e. if log level Trace
 * is set, all messages with a Debug, Info, ... will be logged.
 */
public enum SwCoreLogLevel : Int, CustomStringConvertible
{
    case trace   = 1
    case debug   = 2
    case info    = 4
    case warn    = 8
    case error   = 16
    case fatal   = 32

    public var description : String {
        get {
            switch self  {
            case .trace:
                return "TRACE"

            case .debug:
                return "DEBUG"

            case .info:
                return "INFO"

            case .warn:
                return "WARN"

            case .error:
                return "ERROR"

            case .fatal:
                return "FATAL"
            }
        }
    }
}

/**
 * The execution method determines whether the log command shall
 * be executed synchronuous or asynchronuous. Additionally a
 * dispatch queue must be assigned.
 */
public enum SwCoreLogExecutionMethod {
    /// Execute the log command synchronuously.
    case synchronuous(queue: DispatchQueue)

    /// Execute the log command asynchronuously.
    case asynchronuous(queue: DispatchQueue)
}

/**
 This is the logger class.
 */
public final class SwCoreLogger: SwCoreObject {

    // MARK: - Class Methods

    /// Retrieve the global logger
    ///
    /// - Returns: the global logger instance
    public class func global() -> SwCoreLogger {
        return SwCoreGlobalContext.resolve(service: SwCoreLogger.self, name: "GLOBAL")!
    }

    // MARK: - Public Properties

    /// The name of the logger.
    public private(set) var name: String!

    /// The default log level.
    public              var logLevel: SwCoreLogLevel!

    /// A flag that determines whether this logger is enabled or not.
    public              var enabled: Bool!

    /// The formatter, that transforms a log entry to a string.
    public              var formatter: SwCoreLogFormatter!

    /// Writers that write a log entry to somewhere.
    public              var writers: [SwCoreLogWriter]!

    /// The execution method of the logger.
    public              var executionMethod: SwCoreLogExecutionMethod!


    // MARK: - Initialization

    /**
     * This initializes the logger.
     *
     * - Parameter name: The name of the logger
     * - Parameter logLevel: The default log level
     * - Parameter enabled: A flag that determines whether this logger is enabled or not
     * - Parameter formatter: The formatter, that transforms a log entry to a string
     * - Parameter writers: Writers that write a log entry to somewhere
     * - Parameter executionMethod: The execution method of the logger
     */
    public init(
        name: String,
        logLevel: SwCoreLogLevel = .info,
        enabled: Bool = true,
        formatter: SwCoreLogFormatter = SwCoreLogFormatter(),
        writers: [SwCoreLogWriter] = [SwCoreLogWriter()],
        executionMethod: SwCoreLogExecutionMethod =
            .synchronuous(queue: DispatchQueue.global(qos: DispatchQoS.QoSClass.background))) {
        
        super.init()

        self.name = name
        self.logLevel = logLevel
        self.enabled = enabled
        self.formatter = formatter
        self.writers = writers
        self.executionMethod = executionMethod
    }

    /**
     * This initializes the logger.
     *
     * - Parameter name: The name of the logger
     * - Parameter logLevel: The default log level
     * - Parameter enabled: A flag that determines whether this logger is enabled or not
     * - Parameter formatter: The formatter, that transforms a log entry to a string
     * - Parameter writers: Writers that write a log entry to somewhere
     * - Parameter executionMethod: The execution method of the logger
     */
    public convenience init(
        class: AnyClass,
        logLevel: SwCoreLogLevel = .info,
        enabled: Bool = true,
        formatter: SwCoreLogFormatter = SwCoreLogFormatter(),
        writers: [SwCoreLogWriter] = [SwCoreLogWriter()],
        executionMethod: SwCoreLogExecutionMethod =
            .synchronuous(queue: DispatchQueue.global(qos: DispatchQoS.QoSClass.background))) {

        self.init(
            name: String(describing: `class`.self),
            logLevel: logLevel,
            enabled: enabled,
            formatter: formatter,
            writers: writers,
            executionMethod: executionMethod)
    }
    

    // MARK: - Logger API

    /// Check if the given log level is enabled.
    ///
    /// - Parameters:
    ///   - logLevel: the log level to check
    ///
    /// - Returns: true, if the loglevel is enabled, otherwise false.
    public func isEnabled(logLevel: SwCoreLogLevel) -> Bool {
        return self.logLevel.rawValue <= logLevel.rawValue
    }

    public func log(
        logLevel: SwCoreLogLevel,
        message: String,
        timestamp: Date = Date(),
        filename: String = #file,
        line: Int = #line,
        column: Int = #column,
        function: String = #function) {

        if isEnabled(logLevel: logLevel) && enabled {

            switch executionMethod {
            case .synchronuous(let queue):
                if DispatchQueue.isCurrent(queue) {
                    self.logInternal(logLevel: logLevel,
                                     message: message,
                                     timestamp: timestamp,
                                     filename: filename,
                                     line: line,
                                     column: column,
                                     function: function)
                } else {
                    queue.sync {
                        self.logInternal(logLevel: logLevel,
                                         message: message,
                                         timestamp: timestamp,
                                         filename: filename,
                                         line: line,
                                         column: column,
                                         function: function)
                    }
                }
                break

            case .asynchronuous(let queue):
                queue.async {
                    self.logInternal(logLevel: logLevel,
                                     message: message,
                                     timestamp: timestamp,
                                     filename: filename,
                                     line: line,
                                     column: column,
                                     function: function)
                }
                break

            case .none:
                self.logInternal(logLevel: logLevel,
                                 message: message,
                                 timestamp: timestamp,
                                 filename: filename,
                                 line: line,
                                 column: column,
                                 function: function)
                break
            }
        }
    }

    public func trace(
        message: String,
        timestamp: Date = Date(),
        filename: String = #file,
        line: Int = #line,
        column: Int = #column,
        function: String = #function) {

        log(logLevel: .trace,
            message: message,
            timestamp: timestamp,
            filename: filename,
            line: line,
            column: column,
            function: function)
    }

    public func debug(
        message: String,
        timestamp: Date = Date(),
        filename: String = #file,
        line: Int = #line,
        column: Int = #column,
        function: String = #function) {

        log(logLevel: .debug,
            message: message,
            timestamp: timestamp,
            filename: filename,
            line: line,
            column: column,
            function: function)
    }

    public func info(
        message: String,
        timestamp: Date = Date(),
        filename: String = #file,
        line: Int = #line,
        column: Int = #column,
        function: String = #function) {

        log(logLevel: .info,
            message: message,
            timestamp: timestamp,
            filename: filename,
            line: line,
            column: column,
            function: function)
    }

    public func warn(
        message: String,
        timestamp: Date = Date(),
        filename: String = #file,
        line: Int = #line,
        column: Int = #column,
        function: String = #function) {

        log(logLevel: .warn,
            message: message,
            timestamp: timestamp,
            filename: filename,
            line: line,
            column: column,
            function: function)
    }

    public func error(
        message: String,
        timestamp: Date = Date(),
        filename: String = #file,
        line: Int = #line,
        column: Int = #column,
        function: String = #function) {

        log(logLevel: .error,
            message: message,
            timestamp: timestamp,
            filename: filename,
            line: line,
            column: column,
            function: function)
    }

    public func fatal(
        message: String,
        timestamp: Date = Date(),
        filename: String = #file,
        line: Int = #line,
        column: Int = #column,
        function: String = #function) {

        log(logLevel: .fatal,
            message: message,
            timestamp: timestamp,
            filename: filename,
            line: line,
            column: column,
            function: function)
    }



    // MARK: - Private Methods

    private func logInternal(
        logLevel: SwCoreLogLevel,
        message: String,
        timestamp: Date,
        filename: String,
        line: Int,
        column: Int,
        function: String) {

        let message = SwCoreLogMessage(
            logger: name,
            logLevel: logLevel,
            message: message,
            timestamp: timestamp,
            filename: filename,
            line: line,
            column: column,
            function: function)
        let formattedMessage = formatter.format(message: message)

        writers.forEach { writer in writer.write(message: formattedMessage) }
    }
}

public func trace(
    message: String,
    timestamp: Date = Date(),
    filename: String = #file,
    line: Int = #line,
    column: Int = #column,
    function: String = #function) {

    SwCoreGlobalContext.resolve(service: SwCoreLogger.self, name: "GLOBAL")!.log(
        logLevel: .trace,
        message: message,
        timestamp: timestamp,
        filename: filename,
        line: line,
        column: column,
        function: function)
}

public func debug(
    message: String,
    timestamp: Date = Date(),
    filename: String = #file,
    line: Int = #line,
    column: Int = #column,
    function: String = #function) {

    SwCoreGlobalContext.resolve(service: SwCoreLogger.self, name: "GLOBAL")!.log(
        logLevel: .debug,
        message: message,
        timestamp: timestamp,
        filename: filename,
        line: line,
        column: column,
        function: function)
}

public func info(
    message: String,
    timestamp: Date = Date(),
    filename: String = #file,
    line: Int = #line,
    column: Int = #column,
    function: String = #function) {

    SwCoreGlobalContext.resolve(service: SwCoreLogger.self, name: "GLOBAL")!.log(
        logLevel: .info,
        message: message,
        timestamp: timestamp,
        filename: filename,
        line: line,
        column: column,
        function: function)
}

public func warn(
    message: String,
    timestamp: Date = Date(),
    filename: String = #file,
    line: Int = #line,
    column: Int = #column,
    function: String = #function) {

    SwCoreGlobalContext.resolve(service: SwCoreLogger.self, name: "GLOBAL")!.log(
        logLevel: .warn,
        message: message,
        timestamp: timestamp,
        filename: filename,
        line: line,
        column: column,
        function: function)
}

public func error(
    message: String,
    timestamp: Date = Date(),
    filename: String = #file,
    line: Int = #line,
    column: Int = #column,
    function: String = #function) {

    SwCoreGlobalContext.resolve(service: SwCoreLogger.self, name: "GLOBAL")!.log(
        logLevel: .error,
        message: message,
        timestamp: timestamp,
        filename: filename,
        line: line,
        column: column,
        function: function)
}

public func fatal(
    message: String,
    timestamp: Date = Date(),
    filename: String = #file,
    line: Int = #line,
    column: Int = #column,
    function: String = #function) {

    SwCoreGlobalContext.resolve(service: SwCoreLogger.self, name: "GLOBAL")!.log(
        logLevel: .fatal,
        message: message,
        timestamp: timestamp,
        filename: filename,
        line: line,
        column: column,
        function: function)
}
