/*
   SwCoreContext.swift
   SwallowCore

   Copyright 2020 Swallow.io Development Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import Foundation
import Swinject
import SwinjectAutoregistration


/**
 * This enumeration defines the scopes of services.
 */
public enum SwCoreServiceScope {
    /// A service with scope `singleton` only exists once, i.e. everytime the service
    /// is resolved, the same instance is returned.
    case singleton

    /// When a service with scope `prototype` is resolved, a new instance is created.
    case prototype
}

/** 
 * This is the global context. When implementing a custom context, at least the global
 * context must be its parent.
 */
public let SwCoreGlobalContext: SwCoreContext = { SwCoreGlobalContextInternal() }()


/**
 * A context for an application.
 *
 * A context provides APIs for:
 * - Dependency Injection
 *
 * Author: Thomas Bonk <thomas@meandmymac.de>
 */
open class SwCoreContext: SwCoreObject, Behavior {

    // MARK: - Private Properties

    internal var serviceRegistry: Container!
    internal var registeredServices: [(serviceType: Any, name: String?)] = []


    // MARK: - Public Properties

    /// The parent context. 
    public private(set) var parentContext: SwCoreContext? = nil


    // MARK: - Initialization

    fileprivate init(global: Bool) {
        super.init()

        parentContext = nil
        serviceRegistry = Container(behaviors: [self])
    }

    /**
     * This initializer initializes the context with a parent context. 
     * The parent context is set to the global context by default.
     *
     * Parameter parent: The parent context
     */
    public init(parent: SwCoreContext = SwCoreGlobalContext) {
        super.init()

        parentContext = parent
        serviceRegistry = Container(parent: parentContext!.serviceRegistry, behaviors: [self])
    }


    // MARK: - API Methods

    /**
     * This method resets the service registry. Afterwards all registered services and components
     * are gone.
     */
    public final func resetServiceRegistry() {
        resetServiceRegistryInternal()
    }

    fileprivate func resetServiceRegistryInternal() {
        registeredServices = []
        serviceRegistry = Container(parent: parentContext?.serviceRegistry, behaviors: [self])
    }
}

private class SwCoreGlobalContextInternal: SwCoreContext {

    // MARK: - Inialization

    init() {
        super.init(global: true)
        registerServices()
    }

    override func resetServiceRegistryInternal() {
        super.resetServiceRegistryInternal()
        registerServices()
    }

    private func registerServices() {
        register(service: SwCorePluginManager.self, initializer: SwCorePluginManager.init)
        register(service: SwCoreLocalizationRegistry.self, initializer: SwCoreLocalizationRegistry.init)
        register(service: SwCoreLogger.self) { SwCoreLogger(name: "GLOBAL") }
    }
}
