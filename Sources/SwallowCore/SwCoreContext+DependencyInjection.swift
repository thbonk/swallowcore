/*
   SwCoreContext+DependencyInjection.swift
   SwallowCore

   Copyright 2020 Swallow.io Development Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import Swinject
import SwinjectAutoregistration


public extension SwCoreContext {

    // MARK: - Dependency Injection

    /**
     * This method resolves a service by its type and (optionally) by its name.
     *
     * Parameters:
     * - service serviceType: The type of the service to be resolved
     * - name: The service name
     *
     * Returns: An instance of the service, otherwise nil, if the service wasn't resolved.
     */
    final func resolve<Service>(
        service serviceType: Service.Type, 
                       name: String? = nil) -> Service? {

        let serviceInstance = serviceRegistry.resolve(serviceType, name: name) ?? parentContext?.resolve(service: serviceType, name: name)

        return serviceInstance as Service?
    }

    /**
     * This method resolves all services of the given type.
     *
     * Parameters:
     * - service: Type of the service to be resolved.
     *
     * Returns: An array with all resolved services
     */
    final func resolveAll<Service>(service serviceType: Service.Type) -> [Service] {
        return
            registeredServices
                .filter { 
                    if let type = $0.serviceType as? Service.Type {
                        return type == serviceType 
                    } 
                    return false
                }
                .map { resolve(service: ($0.serviceType as! Service.Type), name: $0.name) }
                .filter { $0 != nil }
                .map { $0! }
            + (parentContext?.resolveAll(service: serviceType) ?? [])
    }
 
    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - factory: The factory lambda that is called for creating an instance of the service
     */
    final func register<Service>(
        service serviceType: Service.Type, 
                       name: String? = nil, 
                      scope: SwCoreServiceScope = .singleton,
                    factory: @escaping (Resolver) -> Service) {

        serviceRegistry
            .register(serviceType, name: name, factory: factory)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
    final func register<Service>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping (()) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }

    /**
     * With this method, a service can be registered by its type and (optionally) by its name.
     * The resolver tries to resolve the parameters of the initializer as services.
     * 
     *
     * Parameters:
     * - service serviceType: The type of the service
     * - name: The name of the service
     * - scope: The scope of the service
     * - initializer: The initializer that shall be called to create an instance of the service
     */
     final func register<Service, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z>(
        service serviceType: Service.Type, 
                    name: String? = nil, 
                    scope: SwCoreServiceScope = .singleton,
                initializer: @escaping ((A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z)) -> Service) {

        serviceRegistry
            .autoregister(serviceType, name: name, initializer: initializer)
            .inObjectScope(map(scope: scope))
    }


    private func map(scope: SwCoreServiceScope) -> ObjectScope {
        switch scope {
        case .singleton:
            return ObjectScope.container

        case .prototype:
            return ObjectScope.transient
        }
    }

    func container<Type, Service>(
        _ container: Container,
        didRegisterType type: Type.Type,
        toService entry: ServiceEntry<Service>,
        withName name: String?) {

        registeredServices.append((serviceType: type, name: name))
    }
}
