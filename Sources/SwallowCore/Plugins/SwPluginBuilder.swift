/*
   SwPluginBuilder.swift
   SwallowCore

   Copyright 2020 Swallow.io Development Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import Foundation


/**
 * This abstract class must be inherited by a builder of a plugin.
 *
 * Plugins must be provided as a dynamic library. In your package manifest,
 * you must define the product as follows:
 * ```swift
 * products: [
 *      .library(name: "MyCoolPlugin", type: .dynamic, targets: ["MyCoolPlugin"]),
 * ]
 * ```
 *
 * Additionally you must implement the function `createPlugin` that instantiates 
 * the plugin builder and returns the instace. The function implementation should 
 * look like this:
 * ```swift
 * @_cdecl("createPlugin")
 * public func createPlugin() -> UnsafeMutableRawPointer {
 *   return Unmanaged.passRetained(PluginABuilder()).toOpaque()
 * }
 * ```
 *
 * Author: Thomas Bonk <thomas@meandmymac.de>
 */
open class SwPluginBuilder<PluginType: SwPlugin>: SwCoreObject {

    // MARK: - Plugin Properties

    /**
     * This method returns the plugin id. The id must be unique and constant;
     * it mustn't change.
     *
     * Returns: The plugin id
     */
    open func pluginId() -> UUID {
        fatalError("You have to override the method pluginId().")
    }

    /**
     * This method returns the plugin name.
     *
     * Returns: The plugin name
     */
    open func pluginName() -> String {
        fatalError("You have to override he method pluginName().")
    }

    /**
     * This method returns the plugin version.
     *
     * Returns: The plugin version
     */
    open func pluginVersion() -> String {
        fatalError("You have to override the method pluginVersion().")
    }
    
    // MARK: - Initialization

    /// Constructor, does nothing in the base class.
    public override init() {
        super.init()
    }

    /**
     * This method must return an instance of the plugin.
     *
     * Returns: An instance of the plugin
     */
    open func build() -> PluginType {
        fatalError("You have to override the method build().")
    }
}
