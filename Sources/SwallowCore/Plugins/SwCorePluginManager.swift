/*
   SwCorePluginManager.swift
   SwallowCore

   Copyright 2020 Swallow.io Development Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import Foundation

private typealias InitFunction = @convention(c) () -> UnsafeMutableRawPointer

/**
 * Errors that can occur when loading a plugin.
 */
public enum SwCorePluginManagerError: Error {
    /**
     * An error occured while opening the plugin.
     *
     * Parameters:
     * - path: The path of the plugin
     * - error: The error description
     */
    case errorWhileOpeningPlugin(path: String, error: String)

    /**
     * An unknown error occured while opening the plugin.
     *
     * Parameters:
     * - path: The path of the plugin
     */
    case unknownErrorWhileOpeningPlugin(path: String)

    /**
     * The function that should return the plugin builder instance
     * hasn't been found in the plugin.
     *
     * Parameters:
     * - symbol: Name of the function
     * - path: The path of the plugin
     */
    case symbolNotFound(symbol: String, path: String)
}

/**
 * The plugin manager is responsible for loading dynmiac libraries as plugins.
 * It can't be instantiated, but it can be injected via the global context.
 *
 * Plugins must be provided as a dynamic library. In your package manifest,
 * you must define the product as follows:
 * ```swift
 * products: [
 *      .library(name: "MyCoolPlugin", type: .dynamic, targets: ["MyCoolPlugin"]),
 * ]
 * ```
 *
 * Additionally you must implement the function `createPlugin` that instantiates 
 * the plugin builder and returns the instace. The function implementation should 
 * look like this:
 * ```swift
 * @_cdecl("createPlugin")
 * public func createPlugin() -> UnsafeMutableRawPointer {
 *   return Unmanaged.passRetained(PluginABuilder()).toOpaque()
 * }
 * ```
 *
 * Swallow plugins should have the file extension `swplugin`.
 *
 * Author: Thomas Bonk <thomas@meandmymac.de>
 */
public final class SwCorePluginManager: SwCoreObject {

    // MARK: - Public Constants

    /// The plugin extension which is `swplugin`
    public static let PluginExtension = "swplugin"


    // MARK: - Initialization

    internal override init() {
        super.init()
    }


    // MARK: - Plugin Manager API

    /**
     * Load a plugin from the given path. If there is an error while loading the plugin,
     * an exception is thrown. It is possible to cache the plugin builder using the 
     * service registry of the global context. The plugin builder is registered with
     * its class and path.
     *
     * Parameters:
     * - atPath path: The path of the plugin
     * - cache: A flag that determines whether the plugin builder shall be chached. If this flag is true,
     *          the plugin manager tries to resolve the plugin builder before trying to load it.
     *
     * Returns: The plugin builder for the the plugin type
     *
     * Throws: An error of type `SwCorePluginManagerError`
     */
    func loadPlugin<PluginType: SwPlugin>(
        atPath path: String,
        cache: Bool = false) throws -> SwPluginBuilder<PluginType> {

        if cache {
            if let pluginBuilder = SwCoreGlobalContext.resolve(service: SwPluginBuilder<PluginType>.self, name: path) {
                return pluginBuilder
            }
        }

        guard let openRes = dlopen(path, RTLD_NOW|RTLD_LOCAL) else {
            if let err = dlerror() {
                throw SwCorePluginManagerError.errorWhileOpeningPlugin(path: path, error: String(format: "%s", err))
            } else {
                throw SwCorePluginManagerError.unknownErrorWhileOpeningPlugin(path: path)
            }
        }

        defer {
            dlclose(openRes)
        }

        let symbolName = "createPlugin"
        
        guard let sym = dlsym(openRes, symbolName) else {
            throw SwCorePluginManagerError.symbolNotFound(symbol: symbolName, path: path)
        }

        let createPlugin/*: InitFunction*/ = unsafeBitCast(sym, to: InitFunction.self)
        let pluginBuilderPointer = createPlugin()
        let builder = Unmanaged<SwPluginBuilder<PluginType>>.fromOpaque(pluginBuilderPointer).takeRetainedValue()

        if cache {
            SwCoreGlobalContext.register(service: SwPluginBuilder<PluginType>.self, name: path) {
                return builder
            }
        }

        return builder
    }

    /**
     * The method loads all plugins in the given path that implement the given
     * plugin type. If there is an error while loading the plugins,
     * an exception is thrown. It is possible to cache the plugin builders using the
     * service registry of the global context. The plugin builders are registered with
     * their classes and paths.
     *
     * Parameters:
     * - atPath path: The path of the plugin
     * - cache: A flag that determines whether the plugin builder shall be chached. If this flag is true,
     *          the plugin manager tries to resolve the plugin builder before trying to load it.
     *
     * Returns: An array of plugin builder for the the plugin type
     *
     * Throws: An error of type `SwCorePluginManagerError
     */
    func loadPlugins<PluginType: SwPlugin>(
        atPath path: String,
        cache: Bool = false) throws -> [SwPluginBuilder<PluginType>] {

        let fm = FileManager.default
        let items = try fm.contentsOfDirectory(atPath: path)

        return try items
            .filter { item in item.hasSuffix(SwCorePluginManager.PluginExtension) }
            .map { item in
                (path: item, builder: try loadPlugin(atPath: item, cache: false) as SwPluginBuilder<PluginType>)
            }
            .filter{ item in type(of: item.builder) == SwPluginBuilder<PluginType>.self }
            .map { item in
                if cache {
                    SwCoreGlobalContext.register(service: SwPluginBuilder<PluginType>.self, name: item.path) {
                        return item.builder
                    }
                }
                return item.builder
            }
    }
}
